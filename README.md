# toc

Table of content of the code released done by SEMCO research team.

The released are ordered by the publication they are referenced in.

## Publications

### Under review

| Title | Conf/Journal | Repository | Released version |
|-------------------|------------------|------------|------------------|
|Formal Specification and Analysis of Software Architecture Attacks using Concurrent Stochastic Games | The 14th Conference on Decision and Game Theory for Security (GameSec-23)  | [semcoproject/semcofdt/swsecurity/threat/stridemps/prism-games](https://gitlab.com/semcoproject/semcofdt/swsecurity/threat/stridemps/prism-games)| [v1.0.0](https://gitlab.com/semcoproject/semcofdt/swsecurity/threat/stridemps/prism-games/-/releases/v1.0.0) |
|Formal Specification and Analysis of Software Architecture under Threats using PRISM | 28th European Symposium on Research in Computer Security (ESORICS) - 2023  | [semcoproject/semcofdt/swsecurity/threat/stridemps/prism-usecase](https://gitlab.com/semcoproject/semcofdt/swsecurity/threat/stridemps/prism-usecase)| [v1.0.0](https://gitlab.com/semcoproject/semcofdt/swsecurity/threat/stridemps/prism-usecase/-/releases/v1.0.0) |

### Published

| Title | Conf/Journal | Repository | Released version |
|-------------------|------------------|------------|------------------|
| A Model-Driven Formal Methods Approach to Software Architectural Security Vulnerabilities Specification and Verification | Journal of Systems and Software Special Issue Model-Driven Engineering for Software Architecture - 2024 | [Alloy Formalization](https://gitlab.com/semcoproject/semcofdt/swsecurity/vulnerability/cwemps/alloy); [Modeling Tool & Use Case](https://gitlab.com/semcoproject/semcomdt/swsecurity/vulnerability/cwe/emft)| |
| Reusable Formal Model Libraries for Specifying and Analyzing Security Objectives in Event-B | 13th International Conference on Model and Data Engineering (MEDI) 2024 | [semcoproject/semcofdt/swsecurity/objective/ciaamps/emft](https://gitlab.com/semcoproject/semcomdt/swsecurity/objective/cbse/emft/-/tree/LThierry) | [LT1.0.0](https://gitlab.com/semcoproject/semcomdt/swsecurity/objective/cbse/emft/-/releases/LT1.0.0) |
| A Formal Approach for Verifying and Validating Security Objectives in Software Architecture | 17th International Conference on Verification and Evaluation of Computer and Communication Systems (VECoS) 2024 | [semcoproject/semcofdt/swsecurity/objective/ciaamps/event-b](https://gitlab.com/semcoproject/semcofdt/swsecurity/objective/ciaamps/event-b) | [v1.4.0](https://gitlab.com/semcoproject/semcofdt/swsecurity/objective/ciaamps/event-b/-/releases/v1.4.0) |
| Interplay of Human Factors and Secure Architecture Design using Model-Driven Engineering | 39th IEEE/ACM International Conference on Automated Software Engineering Workshops (ASEW) - 2024 | [semcoproject/semcomdt/humanfactor/evaluation/questionnaire](https://gitlab.com/semcoproject/semcomdt/humanfactor/evaluation/questionnaire) | [v1.0.0](https://gitlab.com/semcoproject/semcomdt/humanfactor/evaluation/questionnaire/-/tree/v1.0.0) |
| Formalizing the Relationship between Security Policies and Objectives in Software Architectures | International Conference on Software Architecture (ICSA) - 2023 | [semcoproject/semcofdt/swsecurity/objective/ciaamps/coq](https://gitlab.com/semcoproject/semcofdt/swsecurity/objective/ciaamps/coq) |[v1.0.0](https://gitlab.com/semcoproject/semcofdt/swsecurity/objective/ciaamps/coq/-/tree/v1.0.0)|
| Specification and Verification of Communication Paradigms for CBSE in Event B |  International Conference on Engineering of Complex Computer Systems (ICECCS) - 2023 | [semcoproject/semcofdt/swarchitecture/cbsemps/event-b](https://gitlab.com/semcoproject/semcofdt/swarchitecture/cbsemps/event-b) | [v1.0.1](https://gitlab.com/semcoproject/semcofdt/swarchitecture/cbsemps/event-b/-/releases/v1.0.1) |
| Specification, Detection, and Treatment of STRIDE Threats for Software Components: Modeling, Formal Methods, and Tool Support| Journal of Software Architecture (JSA)- 2021 | [semcoproject/semcofdt/swsecurity/threat/stridemps/alloy](https://gitlab.com/semcoproject/semcofdt/swsecurity/threat/stridemps/alloy) |[v1.0.0](https://gitlab.com/semcoproject/semcofdt/swsecurity/threat/stridemps/alloy/-/releases/v1.0.0)|
